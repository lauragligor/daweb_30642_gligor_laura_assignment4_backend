<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    //
    function register(Request $req){
        $user = new User;
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->password = Hash::make($req->input('password'));
        $user->spaces = $req->input('spaces');
        $user->role = $req->input('role');
        $user->save();
        return $user;
    }

    function login(Request $req){
        $user = User::where('email', $req->email)->first();
        if(!$user || !Hash::check($req->password, $user->password)){
            return "ee";
        }
        return $user;
    }

    function edit(Request $req, $id) {
       $user = User::find($id);
       $user->name = $req->input('name');
       $user->save();
       return $user;
    }

    function editm(Request $req, $id) {
       $user = User::find($id);
       $user->email = $req->input('email');
       $user->save();
       return $user;
    }

    function editt(Request $req, $id) {
       $user = User::find($id);
       $user->spaces = $req->input('spaces');
       $user->save();
       return $user;
    }

    function client(User $user){
        return User::where('email', $req->email)->first();
    }

    function file(Request $req, $id){
       $user = User::find($id);
       $user->file = $req->file('file')->store('GDPR');
       $user->save();
       return $user;
       //return "henlo";
    }
}
